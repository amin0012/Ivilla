﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Ivilla.Helper
{
    public static class EnumExtentions
    {
        public static string GetEnumDisplay(this Enum @enum)
        {
            return @enum.GetType()
                        .GetMember(@enum.ToString())
                        .First()
                        .GetCustomAttribute<DisplayAttribute>()
                        .GetName();
        }
    }
}
