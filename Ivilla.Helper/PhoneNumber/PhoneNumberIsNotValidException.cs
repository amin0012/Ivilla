﻿using System;

namespace Ivilla.Helper.PhoneNumber
{
    public class PhoneNumberIsNotValidException : ArgumentException
    {
        public PhoneNumberIsNotValidException(string invalidPhoneNumber) : base($"Phone number '{invalidPhoneNumber}' is not valid.")
        { }
    }
}
