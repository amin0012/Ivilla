﻿using System.Text.RegularExpressions;

namespace Ivilla.Helper.PhoneNumber
{
    public class PhoneNumberNormalizer
    {
        public const string PhoneNumberPattern = @"^((\+|00)?98|0)?(?<number>9\d{9})$";
        private static readonly Regex PhoneNumberRegex = new Regex(PhoneNumberPattern, RegexOptions.Compiled);

        /// <summary>
        /// Removes coutry code and returns in format 9xxxxxxx
        /// </summary>
        /// <exception cref="PhoneNumberIsNotValidException"></exception>
        public static string Normalize(string phoneNumber)
        {
            var match = PhoneNumberRegex.Match(phoneNumber);

            if (!match.Success)
                throw new PhoneNumberIsNotValidException(phoneNumber);

            return match.Groups["number"].Value;
        }
    }
}
