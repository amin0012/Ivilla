﻿using System;
using System.Collections;
using System.Linq;

namespace Ivilla.Helper
{
    public static class Assert
    {
        public static void NotNull<T>(T obj, string name, string message = null) where T : class
        {
            if (obj is null)
                throw new ArgumentNullException(name, message);
        }

        public static void NotNull<T>(T? obj, string name, string message = null) where T : struct
        {
            if (!obj.HasValue)
                throw new ArgumentNullException(name, message);

        }

        public static void NotEmpty<T>(T obj, string name, string message = null, T defaultValue = null) where T : class
        {
            var str = obj as string;

            if (obj == defaultValue || (str != null && string.IsNullOrWhiteSpace(str))
                                    || (obj is IEnumerable list && !list.Cast<object>().Any()))
            {
                throw new ArgumentException("Argument is empty : " + message, name);
            }
        }
    }
}
