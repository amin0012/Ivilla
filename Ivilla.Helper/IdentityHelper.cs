﻿using System;
using System.Security.Claims;
using System.Security.Principal;

namespace Ivilla.Helper
{
    public static class IdentityHelper
    {
        public static int GetUserId(this IIdentity identity)
        {
            var claimsIdentity = identity as ClaimsIdentity;

            var idStr = claimsIdentity?.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (string.IsNullOrEmpty(idStr))
                throw new ArgumentNullException(nameof(idStr));

            return int.Parse(idStr);
        }
    }
}
