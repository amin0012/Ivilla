﻿using System;

namespace Ivilla.Helper
{
   public static class ConvertNumberToStringExtentions
    {

        public static string GetNumberToPersianString(this string txt)
        {
            string ret = " ";
            string[] mainStr = STR_To_Int(txt);
            int q = 0;

            for (int i = mainStr.Length - 1; i >= 0; i--)
            {
                var strva = " ";
                if (ret != " ")
                    strva = " و ";
                ret = Convert_STR(GetCountStr(mainStr[i]), q) + strva + ret;
                q++;
            }
            if (ret == " " || ret == "  ")
                ret = "صفر";
            return ret;
        }

        private static string[] STR_To_Int(string str)
        {
            str = GetCountStr(str);
            string[] ret = new string[str.Length / 3];
            int q = 0;
            for (int I = 0; I < str.Length; I += 3)
            {
                ret[q] = str.Substring(I, 3);
                q++;
            }
            return ret;
        }

        private static string GetCountStr(string str)
        {
            string ret = str;
            int len = (str.Length / 3 + 1) * 3 - str.Length;
            if (len < 3)
            {
                for (int i = 0; i < len; i++)
                {
                    ret = "0" + ret;
                }
            }
            if (ret == "")
                return "000";
            return ret;
        }

        private static string Convert_STR(string INT, int count)
        {
            string ret;
            //یک صد
            if (count == 0)
            {
                if (INT.Substring(1, 1) == "1" && INT.Substring(2, 1) != "0")
                {
                    ret = GET_Number(3, Convert.ToInt32(INT.Substring(0, 1)), " ") + GET_Number(1, Convert.ToInt32(INT.Substring(2, 1)), "");
                }
                else
                {
                    string str = GET_Number(0, Convert.ToInt32(INT.Substring(2, 1)), "");
                    ret = GET_Number(3, Convert.ToInt32(INT.Substring(0, 1)), GET_Number(2, Convert.ToInt32(INT.Substring(1, 1)), "") + str) + GET_Number(2, Convert.ToInt32(INT.Substring(1, 1)), str) + GET_Number(0, Convert.ToInt32(INT.Substring(2, 1)), "");
                }
            }
            //هزار
            else if (count == 1)
            {
                ret = Convert_STR(INT, 0);
                ret += " هزار";
            }
            //میلیون
            else if (count == 2)
            {
                ret = Convert_STR(INT, 0);
                ret += " میلیون";
            }
            //میلیارد
            else if (count == 3)
            {
                ret = Convert_STR(INT, 0);
                ret += " میلیارد";
            }
            //میلیارد
            else if (count == 4)
            {
                ret = Convert_STR(INT, 0);
                ret += " تیلیارد";
            }
            //میلیارد
            else if (count == 5)
            {
                ret = Convert_STR(INT, 0);
                ret += " بیلیارد";
            }
            else
            {
                ret = Convert_STR(INT, 0);
                ret += count.ToString();
            }
            return ret;
        }

        private static string GET_Number(int count, int number, string va)
        {
            string ret = "";

            if (!string.IsNullOrEmpty(va))
            {
                va = " و ";
            }
            if (count == 0 || count == 1)
            {
                bool isDah = Convert.ToBoolean(count);
                string[] myStr = new string[10];
                myStr[1] = isDah ? "یازده" : "یک" + va;
                myStr[2] = isDah ? "دوازده" : "دو" + va;
                myStr[3] = isDah ? "سیزده" : "سه" + va;
                myStr[4] = isDah ? "چهارده" : "چهار" + va;
                myStr[5] = isDah ? "پانزده" : "پنج" + va;
                myStr[6] = isDah ? "شانزده" : "شش" + va;
                myStr[7] = isDah ? "هفده" : "هفت" + va;
                myStr[8] = isDah ? "هجده" : "هشت" + va;
                myStr[9] = isDah ? "نوزده" : "نه" + va;
                return myStr[number];
            }
            if (count == 2)
            {
                string[] myStr = new string[10];
                myStr[1] = "ده";
                myStr[2] = "بیست" + va;
                myStr[3] = "سی" + va;
                myStr[4] = "چهل" + va;
                myStr[5] = "پنجاه" + va;
                myStr[6] = "شصت" + va;
                myStr[7] = "هفتاد" + va;
                myStr[8] = "هشتاد" + va;
                myStr[9] = "نود" + va;
                return myStr[number];
            }
            if (count == 3)
            {
                string[] myStr = new string[10];
                myStr[1] = "یکصد" + va;
                myStr[2] = "دویست" + va;
                myStr[3] = "سیصد" + va;
                myStr[4] = "چهارصد" + va;
                myStr[5] = "پانصد" + va;
                myStr[6] = "ششصد" + va;
                myStr[7] = "هفتصد" + va;
                myStr[8] = "هشتصد" + va;
                myStr[9] = "نهصد" + va;
                return myStr[number];
            }
            return ret;
        }
    }
}
