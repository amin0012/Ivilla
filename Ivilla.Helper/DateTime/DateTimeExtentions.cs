﻿using System.Globalization;

namespace Ivilla.Helper.DateTime
{
    public static class DateTimeExtentions
    {
        public static string ToPersianDateString(this System.DateTime date)
        {
            var culture = new CultureInfo("fa-IR");

            return date.ToString("HH:mm yyyy/MM/dd", culture);
        }

        public static string ToShortPersianDateString(this System.DateTime date)
        {
            var culture = new CultureInfo("fa-IR");

            return date.ToString("yyyy/MM/dd", culture);
        }

        public static int ToAge(this System.DateTime birthDate)
        {
            var today = System.DateTime.Today;
            var age = today.Year - birthDate.Year;

            if (birthDate.Month > today.Month && birthDate.DayOfYear > today.DayOfYear)
                age--;

            return age;
        }

        public static System.DateTime CreateDateFromPersianDate(int year, int month, int day)
        {
            var calendar = new PersianCalendar();

            return calendar.ToDateTime(year, month, day, 0, 0, 0, 0);
        }

        public static int GetPersianYear(this System.DateTime date)
        {
            var calendar = new PersianCalendar();

            return calendar.GetYear(date);
        }

        public static int GetPersianMonth(this System.DateTime date)
        {
            var calendar = new PersianCalendar();

            return calendar.GetMonth(date);
        }

        public static int GetPersianDay(this System.DateTime date)
        {
            var calendar = new PersianCalendar();

            return calendar.GetDayOfMonth(date);
        }
    }
}
