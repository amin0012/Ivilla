﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Ivilla.WebFramework
{
    public abstract class BaseViewModel<TViewModel, TEntity, TKey> where TViewModel : class
                                                                   where TEntity : class
    {
        [Display(Name = "ردیف")]
        public TKey Id { get; set; }

        public Task<SelectList> ComboAsync()
        {
            return null;
        }

        /// <summary>
        /// Maps the specified view model to a entity object.
        /// </summary>
        public TEntity ToEntity()
        {
            return Mapper.Map<TEntity>(CastToDerivedClass(this));
        }

        public TEntity ToEntity(TEntity entity)
        {
            return Mapper.Map(CastToDerivedClass(this), entity);
        }

        public TViewModel FromEntity(TEntity model)
        {
            return Mapper.Map<TViewModel>(model);
        }

        protected TViewModel CastToDerivedClass(BaseViewModel<TViewModel, TEntity, TKey> baseInstance)
        {
            return Mapper.Map<TViewModel>(baseInstance);
        }
    }
}
