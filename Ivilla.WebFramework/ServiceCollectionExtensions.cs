﻿using Ivilla.Data;
using Ivilla.Data.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System.Security.Principal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Ivilla.Repository;
using Microsoft.EntityFrameworkCore;

namespace Ivilla.WebFramework
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDbContext(this IServiceCollection services,IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(optionsBuilder =>
            {
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("SqlServer"))
                              .ConfigureWarnings(warning => warning.Throw(RelationalEventId.QueryClientEvaluationWarning));
            });
        }

        public static void AddServices(this IServiceCollection services)
        {
            //Singleton
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();// HttpContextAccessor => IHttpContextAccessor
            //Transient
            services.AddTransient<IPrincipal>(provider => provider.GetService<IHttpContextAccessor>().HttpContext.User);
            //Scoped
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
        }
    }
}
