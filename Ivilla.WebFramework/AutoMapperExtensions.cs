﻿using AutoMapper;
using System.Linq;
using System.Reflection;

namespace Ivilla.WebFramework
{
    public static class AutoMapperExtensions
    {
        public static void InitializeAutoMapper()
        {
            Mapper.Initialize(configuration =>
            {
                configuration.ConfigureAutoMapperForViewModels();
            });
        }

        public static void ConfigureAutoMapperForViewModels(this IMapperConfigurationExpression config)
        {
            config.ConfigureAutoMapperForViewModels(Assembly.GetEntryAssembly());
        }

        public static void ConfigureAutoMapperForViewModels(this IMapperConfigurationExpression config, Assembly assembly)
        {
            var alltypes = assembly.GetTypes();

            var viewModelTypes = alltypes.Where(type =>
                                                type.IsClass && type.BaseType != null && type.BaseType.IsGenericType &&
                                                type.BaseType.GetGenericTypeDefinition() == typeof(BaseViewModel<,,>))
                                         .Select(type =>
                                         {
                                             var arguments = type.BaseType.GetGenericArguments();
                                             // var interfaces = type.GetInterfaces();
                                             return new
                                             {
                                                 ViewModel = arguments[0],
                                                 Entity = arguments[1],
                                             };
                                         }).ToList();

            foreach (var type in viewModelTypes)
            {
                config.CreateMap(type.Entity, type.ViewModel).ReverseMap();
            }
        }
    }
}
