﻿using Ivilla.Data.EntityConfig;
using Ivilla.Domain.Entity;
using Ivilla.Domain.Entity.Common;
using Ivilla.Helper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Ivilla.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);     
            // TODO: Do With Reflection

            modelBuilder.ApplyConfiguration(new UserConfig());
            modelBuilder.ApplyConfiguration(new RoleConfig());
			modelBuilder.Entity<User>().ToTable(nameof(User)).HasQueryFilter(a=>!a.IsDeleted);
			modelBuilder.Entity<Role>().ToTable(nameof(Role)).HasQueryFilter(a => !a.IsDeleted);
			base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            _cleanString();

            _setAuditEntityProperties();

            ChangeTracker.DetectChanges();

            ChangeTracker.AutoDetectChangesEnabled = false; // for performance reasons, to avoid calling DetectChanges() again.

            var result = base.SaveChanges();

            ChangeTracker.AutoDetectChangesEnabled = true;

            return result;
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            _cleanString();

            _setAuditEntityProperties();

            ChangeTracker.DetectChanges();

            ChangeTracker.AutoDetectChangesEnabled = false; // for performance reasons, to avoid calling DetectChanges() again.

            var result = base.SaveChanges(acceptAllChangesOnSuccess);

            ChangeTracker.AutoDetectChangesEnabled = true;

            return result;
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            _cleanString();

            _setAuditEntityProperties();

            ChangeTracker.DetectChanges();

            ChangeTracker.AutoDetectChangesEnabled = false; // for performance reasons, to avoid calling DetectChanges() again.

            var result = base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);

            ChangeTracker.AutoDetectChangesEnabled = true;

            return result;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            _cleanString();

            _setAuditEntityProperties();

            ChangeTracker.DetectChanges();

            ChangeTracker.AutoDetectChangesEnabled = false; // for performance reasons, to avoid calling DetectChanges() again.

            var result = base.SaveChangesAsync(cancellationToken);

            ChangeTracker.AutoDetectChangesEnabled = true;

            return result;
        }

        private void _cleanString()
        {
            var changedEntities = ChangeTracker.Entries().Where(x => x.State == EntityState.Added || x.State == EntityState.Modified);

            foreach (var item in changedEntities)
            {
                if (item.Entity == null)
                    continue;

                var properties = item.Entity.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                                      .Where(p => p.CanRead && p.CanWrite && p.PropertyType == typeof(string));

                foreach (var property in properties)
                {
                    //var propName = property.Name;
                    var val = (string)property.GetValue(item.Entity, null);

                    if (val.HasValue())
                    {
                        var newVal = val.Fa2En().FixPersianChars();

                        if (newVal == val)
                            continue;

                        property.SetValue(item.Entity, newVal, null);
                    }
                }
            }
        }

        private void _setAuditEntityProperties()
        {
            //--Get User Id If Authenticated.
            int? userId = _getUserId();

            var entityEntries = ChangeTracker.Entries<BaseEntity>().Where(x => x.Entity.GetType()
                                             .IsAssignableFrom(typeof(BaseEntity))
                                              && (x.State == EntityState.Added
                                               || x.State == EntityState.Modified
                                               || x.State == EntityState.Deleted));

            foreach (var entry in entityEntries)
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Entity.CreatedUserId = userId;
                    entry.Entity.CreatedDate = DateTimeOffset.Now;
                }
                if (entry.State == EntityState.Modified)
                {
                    entry.Entity.ModifiedUserId = userId;
                    entry.Entity.ModifiedDate = DateTimeOffset.Now;
                }
            }
        }

        private int? _getUserId()
        {
            //Error Mide Zaman Add Kardane User Bekhay User Id Ro Begiri Az Identity.Chon Claim Bad Az Add Shodane User Anjam Mishe.
            var userIdValue = "";// HttpContextAccessor.HttpContext?.User?.Identity?.GetUserId().ToString();

            if (userIdValue.HasValue())
                return int.Parse(userIdValue);

            return null;
        }

    }
}
