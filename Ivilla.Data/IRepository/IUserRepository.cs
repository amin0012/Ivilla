﻿using System.Threading;
using System.Threading.Tasks;
using Ivilla.Domain.Entity;

namespace Ivilla.Data.IRepository
{
    public interface IUserRepository
    {
        Task<User> GetUserByPhoneNumberAsync(string phoneNumber);

        Task<bool> ExistByPhoneNumberAsync(string phoneNumber);

        Task AddUserAsync(User user, CancellationToken cancellationToken);

        Task EditUserAsync(User user, CancellationToken cancellationToken);

        Task<User> GetUserByIdAsync(int id);
    }
}
