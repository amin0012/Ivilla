﻿namespace Ivilla.Data.Contracts
{
    public interface IDataInitializer : IScopedDependency
    {
        void SeedData();
    }
}