﻿using Ivilla.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ivilla.Data.EntityConfig
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id).ValueGeneratedOnAdd();

            builder.Property(c => c.FirstName).HasColumnType("nvarchar(30)")
                                              .HasMaxLength(30)
                                              .IsRequired();

            builder.Property(c => c.LastName).HasColumnType("nvarchar(30)")
                                             .HasMaxLength(30)
                                             .IsRequired();

            builder.Property(c => c.PhoneNumber).HasColumnType("char(10)")
                                                .HasMaxLength(10)
                                                .IsRequired();

            builder.Property(c => c.NationalCode).HasColumnType("char(10)")
                                                 .HasMaxLength(10);            
        }
    }
}
