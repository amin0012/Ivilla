﻿using Ivilla.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ivilla.Data.EntityConfig
{
    public class RoleConfig : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable("Roles");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Name).HasColumnType("varchar(30)")
                                         .HasMaxLength(30)
                                         .IsRequired();

            builder.Property(c => c.PersianName).HasColumnType("nvarchar(30)")
                                                .HasMaxLength(30)
                                                .IsRequired();

            builder.HasMany(c => c.Users)
                   .WithOne(c => c.Role)
                   .HasForeignKey(c => c.RoleId)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
