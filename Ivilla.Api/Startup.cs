﻿using Ivilla.Api.Providers;
using Ivilla.Data;
using Ivilla.Data.Contracts;
using Ivilla.WebFramework;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace Ivilla.Api
{
	public class Startup
	{
		public IConfiguration Configuration { get; }

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;

			AutoMapperExtensions.InitializeAutoMapper();
		}

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMemoryCache();

			services.AddOptions();

			services.AddDbContext(Configuration);

			// Add EF services to the services container.
			string conString = Microsoft
		   .Extensions
		   .Configuration
		   .ConfigurationExtensions
		   .GetConnectionString(this.Configuration, "SqlServer");
			services.AddEntityFrameworkSqlServer()
			   .AddDbContext<ApplicationDbContext>(options =>
				  options.UseSqlServer(conString));


			//services.Configure<ConfigurationSettings>(Configuration.GetSection("ProjectConfiguration"));

			services.Configure<CookiePolicyOptions>(options =>
			{
				options.CheckConsentNeeded = context => true;
				options.MinimumSameSitePolicy = SameSiteMode.None;
			});

			services.AddServices();

			services.AddMvc(options =>
			{
			}).SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddControllersAsServices();

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info
				{
					Version = "v1",
					Title = "Ivilla API Documentation",
					Description = "ASP.NET Core Web API"
				});
			});
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env,
							  IServiceScopeFactory serviceScopeFactory)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseDatabaseErrorPage();
				app.UseSwagger();
				app.UseSwaggerUI(c =>
				{
					c.SwaggerEndpoint("/swagger/v1/swagger.json", "Ivilla API V1");
				});
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				app.UseHsts();
				//TODO: Log Error
			}

			using (var scope = serviceScopeFactory.CreateScope())
			{
				var dataInitializers = scope.ServiceProvider.GetServices<IDataInitializer>();

				foreach (var initializer in dataInitializers)
					initializer.SeedData();
			}

			app.UseHttpsRedirection();

			app.UseCookiePolicy();

			app.UseAuthentication();

			app.UseCorsMiddleware();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");

				//routes.MapRoute(
				//    name: "areas",
				//    template: "{area:exists}/{controller=Account}/{action=Index}/{id?}");
			});
		}
	}
}
