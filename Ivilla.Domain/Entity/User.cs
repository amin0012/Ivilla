﻿using Ivilla.Domain.Entity.Common;
using Ivilla.Domain.EnumType;
using Ivilla.Domain.Exception.Users;
using System;

namespace Ivilla.Domain.Entity
{
    public class User : BaseEntity
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string NationalCode { get; set; }

        public UserStatus Status { get; set; }

        public DateTimeOffset? LastVisitDateTime { get; set; }

        public int RoleId { get; set; }

        public virtual Role Role { get; set; }

        public string DisplayName
        {
            get
            {
                var displayName = $"{FirstName} {LastName}";

                return string.IsNullOrWhiteSpace(displayName) ? throw new FirstNameOrLastNameNotProvidedException() : displayName;
            }
        }
    }
}
