﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ivilla.Domain.Entity.Common
{
    public interface IBaseEntity : IBaseAllEntity
    {
        DateTimeOffset CreatedDate { get; set; }

        DateTimeOffset? ModifiedDate { get; set; }

        int? CreatedUserId { get; set; }

        [ForeignKey(nameof(CreatedUserId))]
        User Creator { get; set; }

        int? ModifiedUserId { get; set; }

        [ForeignKey(nameof(ModifiedUserId))]
        User Modifier { get; set; }
    }
}