﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ivilla.Domain.Entity.Common
{
    public abstract class BaseEntity : IBaseEntity, ICommonEntity
    {
        public DateTimeOffset CreatedDate { get; set; }

        public DateTimeOffset? ModifiedDate { get; set; }

        public int? CreatedUserId { get; set; }

        [ForeignKey(nameof(CreatedUserId))]
        public virtual User Creator { get; set; }

        public int? ModifiedUserId { get; set; }

        [ForeignKey(nameof(ModifiedUserId))]
        public virtual User Modifier { get; set; }

        public bool IsDeleted { get; set; }
    }
}