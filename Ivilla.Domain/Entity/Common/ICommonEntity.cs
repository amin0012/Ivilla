﻿
namespace Ivilla.Domain.Entity.Common
{
    public interface ICommonEntity : IBaseAllEntity
    {
        bool IsDeleted { get; set; }
    }
}
