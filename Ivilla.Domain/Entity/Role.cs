﻿using Ivilla.Domain.Entity.Common;
using System;
using System.Collections.Generic;

namespace Ivilla.Domain.Entity
{
    public class Role : IBaseAllEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string PersianName { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public DateTimeOffset? ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }

        public virtual ICollection<User> Users { get; set; }

    }
}
