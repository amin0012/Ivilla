﻿
namespace Ivilla.Domain.Exception.Users
{
    public class FirstNameOrLastNameNotProvidedException : BusinessException
    {
        public FirstNameOrLastNameNotProvidedException() : base("First Name Or Last Name Not Provided Exception")
        {
        }
    }
}
