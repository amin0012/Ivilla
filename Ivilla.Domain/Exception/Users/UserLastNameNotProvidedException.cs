﻿namespace Ivilla.Domain.Exception.Users
{
    public class UserLastNameNotProvidedException : BusinessException
    {
        public UserLastNameNotProvidedException() : base("User LastName is not provided")
        {
        }
    }
}
