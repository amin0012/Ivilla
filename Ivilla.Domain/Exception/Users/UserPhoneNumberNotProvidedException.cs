﻿namespace Ivilla.Domain.Exception.Users
{
    public class UserPhoneNumberNotProvidedException : BusinessException
    {
        public UserPhoneNumberNotProvidedException() : base("User PhoneNumber is not provided")
        {
        }
    }
}
