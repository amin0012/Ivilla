﻿namespace Ivilla.Domain.Exception.Users
{
    public class UserFirstNameNotProvidedException:BusinessException
    {
        public UserFirstNameNotProvidedException() : base("User FirstName is not provided")
        {
        }
    }
}
