﻿namespace Ivilla.Domain.Exception.Users
{
    public class UserPhoneNumberIsAlreadyTakenException : BusinessException
    {
        public UserPhoneNumberIsAlreadyTakenException() : base("User PhoneNumber is already taken")
        {
        }
    }
}
