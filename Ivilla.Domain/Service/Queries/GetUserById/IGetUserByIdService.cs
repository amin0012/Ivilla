﻿using System.Threading.Tasks;

namespace Ivilla.Domain.Service.Queries.GetUserById
{
    public interface IGetUserByIdService
    {
        Task<GetUserByIdResponse> ExecuteAsync(GetUserByIdRequest request);
    }
}
