﻿namespace Ivilla.Domain.Service.Queries.GetUserById
{
    public class GetUserByIdRequest
    {
        public int Id { get; set; }
    }
}
