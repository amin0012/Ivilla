﻿using Ivilla.Domain.EnumType;

namespace Ivilla.Domain.Service.Queries.GetUserByPhoneNumber
{
    public class GetUserByPhoneNumberResponse
    {
        public int Id { get; set; }

        public string PhoneNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string NationalCode { get; set; }

        public UserStatus Status { get; set; }

        public string RoleName { get; set; }
    }
}
