﻿using System.Threading.Tasks;

namespace Ivilla.Domain.Service.Queries.GetUserByPhoneNumber
{
    public interface IGetUserByPhoneNumberService
    {
        Task<GetUserByPhoneNumberResponse> ExecuteAsync(GetUserByPhoneNumberRequest request);
    }
}
