﻿namespace Ivilla.Domain.Service.Queries.GetUserByPhoneNumber
{
    public class GetUserByPhoneNumberRequest
    {
        public string PhoneNumber { get; set; }
    }  
}
