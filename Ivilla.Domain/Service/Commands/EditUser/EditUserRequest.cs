﻿using System;
using Ivilla.Domain.EnumType;

namespace Ivilla.Domain.Service.Commands.EditUser
{
    public class EditUserRequest
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string NationalCode { get; set; }

        public UserStatus Status { get; set; }

        public DateTimeOffset? LastVisitDateTime { get; set; }
    }
}
