﻿using System.Threading;
using System.Threading.Tasks;

namespace Ivilla.Domain.Service.Commands.EditUser
{
    public interface IEditUserService
    {
        Task ExecuteAsync(EditUserRequest request, CancellationToken cancellationToken);
    } 
}
