﻿using Ivilla.Domain.EnumType;

namespace Ivilla.Domain.Service.Commands.AddUser
{
    public class AddUserRequest
    {
        public string PhoneNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public UserStatus Succeeded { get; } = UserStatus.Active;
    }  
}
