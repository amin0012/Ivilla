﻿using System;
using Ivilla.Domain.EnumType;

namespace Ivilla.Domain.Service.Commands.AddUser
{
    public class AddUserResponse
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTimeOffset? CreatedDateTime { get; set; }

        public DateTimeOffset? LastVisitDateTime { get; set; }

        public string PhoneNumber { get; set; }

        public UserStatus Status { get; set; }
    }
}
