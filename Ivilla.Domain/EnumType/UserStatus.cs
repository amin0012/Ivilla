﻿using System.ComponentModel.DataAnnotations;

namespace Ivilla.Domain.EnumType
{
    public enum UserStatus
    {
        [Display(Name = "فعال")]
        Active = 0,

        [Display(Name = "غیر فعال")]
        Deactive = 1
    }
}
