﻿using System.ComponentModel.DataAnnotations;

namespace Ivilla.Domain.EnumType
{
    public enum RoleType
    {
        [Display(Name = "مدیر ارشد")]
        SuperAdmin = 0,

        [Display(Name = "مدیر")]
        Admin = 1,

        [Display(Name = "کاربر")]
        User = 2
    }
}
