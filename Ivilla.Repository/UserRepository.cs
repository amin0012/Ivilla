﻿using Dapper;
using Ivilla.Data;
using Ivilla.Data.IRepository;
using Ivilla.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ivilla.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext dbContext, IConfiguration configuration)
               : base(dbContext, configuration)
        {
        }

        public async Task<User> GetUserByPhoneNumberAsync(string phoneNumber)
        {
            var query = $@"SELECT 
                           Id,
                           FirstName, 
                           LastName, 
                           PhoneNumber, 
                           NationalCode, 
                           Status, 
                           LastVisitDateTime, 
                           RoleId 
                           FROM {nameof(Entities)} AS U
                           INNER JOIN {nameof(DbContext.Roles)} AS R
                           ON R.Id == U.RoleId
                           WHERE PhoneNumber = {phoneNumber}";

            using (IDbConnection conn = DapperConnection)
            {
                conn.Open();

                var result = await conn.QueryAsync<User>(query, new { PhoneNumber = phoneNumber });

                return result.FirstOrDefault();
            }
        }

        public async Task<bool> ExistByPhoneNumberAsync(string phoneNumber)
        {
            return await Entities.AnyAsync(x => x.PhoneNumber == phoneNumber);
        }

        public async Task AddUserAsync(User user, CancellationToken cancellationToken)
        {
            await AddAsync(user, cancellationToken);
        }

        public async Task EditUserAsync(User user, CancellationToken cancellationToken)
        {
            await UpdateAsync(user, cancellationToken);
        }

        public async Task<User> GetUserByIdAsync(int id)
        {
            var query = $@"SELECT 
                           Id,
                           FirstName, 
                           LastName, 
                           PhoneNumber, 
                           NationalCode, 
                           Status, 
                           LastVisitDateTime, 
                           RoleId 
                           FROM {nameof(Entities)} AS U
                           INNER JOIN {nameof(DbContext.Roles)} AS R
                           ON R.Id == U.RoleId
                           WHERE Id = {id}";

            using (IDbConnection conn = DapperConnection)
            {
                conn.Open();

                var result = await conn.QueryAsync<User>(query, new { Id = id });

                return result.SingleOrDefault();
            }
        }
    }
}
