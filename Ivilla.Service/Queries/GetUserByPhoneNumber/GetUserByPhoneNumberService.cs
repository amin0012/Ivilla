﻿using System.Threading.Tasks;
using Ivilla.Data.IRepository;
using Ivilla.Domain.Service.Queries.GetUserByPhoneNumber;

namespace Ivilla.Service.Queries.GetUserByPhoneNumber
{
    public class GetUserByPhoneNumberService : IGetUserByPhoneNumberService
    {
        private IUserRepository UserRepository { get; }

        public GetUserByPhoneNumberService(IUserRepository userRepository)
        {
            UserRepository = userRepository;
        }

        public async Task<GetUserByPhoneNumberResponse> ExecuteAsync(GetUserByPhoneNumberRequest request)
        {
            GetUserByPhoneNumberResponse result = null;

            var user = await UserRepository.GetUserByPhoneNumberAsync(request.PhoneNumber);

            if (user != null)
                result = GetUserByPhoneNumberFactory.Create(user);

            return result;
        }
    }
}
