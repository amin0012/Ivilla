﻿using Ivilla.Domain.Entity;
using Ivilla.Domain.EnumType;
using Ivilla.Domain.Service.Queries.GetUserByPhoneNumber;

namespace Ivilla.Service.Queries.GetUserByPhoneNumber
{
    public class GetUserByPhoneNumberFactory
    {
        public static GetUserByPhoneNumberResponse Create(User user)
        {
            return new GetUserByPhoneNumberResponse
            {
                Id = user.Id,
                PhoneNumber = user.PhoneNumber,
                FirstName = user.FirstName,
                LastName = user.LastName,
                NationalCode = user.NationalCode ?? string.Empty,
                Status = user.Status,
                RoleName = ((RoleType)user.Role.Id).ToString()
            };
        }
    }
}
