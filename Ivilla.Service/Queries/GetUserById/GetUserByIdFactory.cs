﻿using Ivilla.Domain.Entity;
using Ivilla.Domain.EnumType;
using Ivilla.Domain.Service.Queries.GetUserById;

namespace Ivilla.Service.Queries.GetUserById
{
    public class GetUserByIdFactory
    {
        public static GetUserByIdResponse Create(User user)
        {
            return new GetUserByIdResponse
            {
                Id = user.Id,
                PhoneNumber = user.PhoneNumber,
                FirstName = user.FirstName,
                LastName = user.LastName,
                NationalCode = user.NationalCode ?? string.Empty,
                Status = user.Status,
                RoleName = ((RoleType)user.Role.Id).ToString()
            };
        }
    }
}
