﻿using System.Threading.Tasks;
using Ivilla.Data.IRepository;
using Ivilla.Domain.Service.Queries.GetUserById;

namespace Ivilla.Service.Queries.GetUserById
{
    public class GetUserByIdService : IGetUserByIdService
    {
        private IUserRepository UserRepository { get; }
        public GetUserByIdService(IUserRepository userRepository)
        {
            UserRepository = userRepository;
        }

        public async Task<GetUserByIdResponse> ExecuteAsync(GetUserByIdRequest request)
        {
            var user = await UserRepository.GetUserByIdAsync(request.Id);

            if (user == null)
                return null;

            var result = GetUserByIdFactory.Create(user);

            return result;
        }
    }
}
