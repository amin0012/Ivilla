﻿using Ivilla.Data.IRepository;
using Ivilla.Domain.Exception.Users;
using Ivilla.Domain.Service.Commands.AddUser;

namespace Ivilla.Service.Commands.AddUser
{
    public class EditUserValidation
    {
        public static void CheckValidation(AddUserRequest request, IUserRepository userRepository)
        {
            if (string.IsNullOrWhiteSpace(request.PhoneNumber))
                throw new UserPhoneNumberNotProvidedException();

            if (string.IsNullOrWhiteSpace(request.FirstName))
                throw new UserFirstNameNotProvidedException();

            if (string.IsNullOrWhiteSpace(request.LastName))
                throw new UserLastNameNotProvidedException();

            if (userRepository.ExistByPhoneNumberAsync(request.PhoneNumber).Result)
                throw new UserPhoneNumberIsAlreadyTakenException();
        }
    }
}
