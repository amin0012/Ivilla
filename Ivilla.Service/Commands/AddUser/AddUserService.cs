﻿using System.Threading;
using System.Threading.Tasks;
using Ivilla.Data.IRepository;
using Ivilla.Domain.Service.Commands.AddUser;

namespace Ivilla.Service.Commands.AddUser
{
    public class AddUserService : IAddUserService
    {
        private IUserRepository UserRepository { get; }

        public AddUserService(IUserRepository userRepository)
        {
            UserRepository = userRepository;
        }

        public async Task ExecuteAsync(AddUserRequest request, CancellationToken cancellationToken)
        {
            AddUserValidation.CheckValidation(request, UserRepository);

            var user = AddUserFactory.Create(request);

           await UserRepository.AddUserAsync(user, cancellationToken);
        }
    }
}
