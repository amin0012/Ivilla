﻿using Ivilla.Domain.Entity;
using Ivilla.Domain.EnumType;
using Ivilla.Domain.Service.Commands.AddUser;
using Ivilla.Helper.PhoneNumber;

namespace Ivilla.Service.Commands.AddUser
{
    public class AddUserFactory
    {
        public static User Create(AddUserRequest request)
        {
            return new User
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                PhoneNumber = PhoneNumberNormalizer.Normalize(request.PhoneNumber),
                Status = request.Succeeded,
                RoleId = RoleType.User.GetHashCode(),
            };
        }
    }
}
