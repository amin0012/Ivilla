﻿using System.Threading;
using System.Threading.Tasks;
using Ivilla.Data.IRepository;
using Ivilla.Domain.Service.Commands.EditUser;

namespace Ivilla.Service.Commands.EditUser
{
    public class EditUserService : IEditUserService
    {
        private IUserRepository UserRepository { get; }

        public EditUserService(IUserRepository userRepository)
        {
            UserRepository = userRepository;
        }

        public async Task ExecuteAsync(EditUserRequest request, CancellationToken cancellationToken)
        {
            var user = await UserRepository.GetUserByIdAsync(request.Id);

            EditUserFactory.EditUser(ref user, request);

            await UserRepository.EditUserAsync(user, cancellationToken);
        }
    }
}
