﻿using System;
using Ivilla.Domain.Entity;
using Ivilla.Domain.Service.Commands.EditUser;

namespace Ivilla.Service.Commands.EditUser
{
    public class EditUserFactory
    {
        public static void EditUser(ref User user, EditUserRequest request)
        {
            user.FirstName = request.FirstName ?? user.FirstName;
            user.LastName = request.LastName ?? user.LastName;
            user.NationalCode = request.NationalCode ?? user.NationalCode;
            //user.Status = request.Status.ToString() ? request.Status.ToString() : user.Status.ToString();
            user.LastVisitDateTime = DateTimeOffset.Now;
        }
    }
}
